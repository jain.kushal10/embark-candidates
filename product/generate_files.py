import csv
 
filename = "analytics-profiles.csv"
 
fields = []
rows = []
 
with open(filename, 'r') as csvfile:
    csvreader = csv.reader(csvfile)
     
    fields = next(csvreader)
 
    for row in csvreader:
        rows.append(row)
o = ''
for row in rows:
    skills = row[6].split('|')
    # content = '<!DOCTYPE html><html lang="en"><head> <meta charset="UTF-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta http-equiv="Content-Type" content="text/html charset=UTF-8"/> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css"> <link rel="preconnect" href="https://fonts.googleapis.com"> <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> <title>Document</title></head><body style="font-family: \'Poppins\', sans-serif;"> <div style="padding: 24px; background-color: #fff2e8;max-width: 550px;border-radius: 10px;margin: 5px;box-shadow: 0px 0px 10px #ccc;"> <div> <div style="display: flex; align-content: space-between; width: 100%" class="mb-2"> <h4 style="margin-bottom: 0px; margin-top: 0px"> <b>'+ row[0] +'</b> </h4> &nbsp;&nbsp; <a href="' + row[3] + '" target="_blank"> <img src="https://cdn-icons-png.flaticon.com/512/174/174857.png" alt="" height="24"> </a> </div><div> <span><small style="color: #666;">Available for</small> '+ row[4] +'</span> </div></div><div class="pt-3 docs-outer"> <div> <a href="'+ row[1] +'" target="_blank" style="cursor: pointer;"><button type="button" class="btn btn-danger">Video Resume</button></a> &nbsp; <a href="'+ row[2] +'" target="_blank" style="cursor: pointer;"> <button type="button" class="btn btn-primary" style="background-color: #6558f5;border-color: #6558f5;">Resume</button></a>&nbsp; </div><br><h6 style="margin-bottom: 10px;"> Based on our evaluation,'+ row[5] +'\'s top skills are: </h6> <div style="line-height: 2.4rem;"> <span style="padding: 3px 15px; background-color: #2979ff; border-radius: 100px; color: white; white-space:nowrap;">'+ skills[0] +'</span> <span style="padding: 3px 15px; background-color: #2979ff; border-radius: 100px; color: white; white-space:nowrap;">'+ skills[1] +'</span> <span style="padding: 3px 15px; background-color: #2979ff; border-radius: 100px; color: white; white-space:nowrap;">'+ skills[2] +'</span> </div></div></div><span>&nbsp;</span></body></html>'
    # f = open(row[0] + '.html', "a")
    # f.write(content)
    # f.close()
    content = '<li><a href="'+ row[0] +'.html">'+ row[0] +'</a></li>'
    o += content

print(o)